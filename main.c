#include <stdio.h>

#include "learn_fn.h"

// stadarni naziv ulaznih parametara za main fukciju: int argc, char** argv
int main(int argument_counter, char** argument_values) {
  int rezultat_1 = fn_1(2, 2, '+');
  int rezultat_2 = fn_1(3, 2, '-');

  print_dvije_vrijednosti(rezultat_1, rezultat_2);

  double rezultat_3 = izracunaj_double(3.0);
  double rezultat_4 = izracunaj_double(7.0);

  print_dvije_vrijednosti(rezultat_3, rezultat_4);

  return 1;
}

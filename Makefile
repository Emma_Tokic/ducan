C_COMPILER=gcc
C_FLAGS=-g -Wall

store: main.o learn_fn.o
	$(C_COMPILER) main.o learn_fn.o -o learn_fn.exe

main.o: main.c
	$(C_COMPILER) $(C_FLAGS) -c main.c

learn_fn.o: learn_fn.c
	$(C_COMPILER) $(C_FLAGS) -c learn_fn.c

clear:
	$(RM) *.exe *.o

